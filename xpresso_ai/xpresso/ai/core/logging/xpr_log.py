""" Log management for Xpresso Project"""

__all__ = ['XprLogger']
__author__ = 'Srijan Sharma'

import datetime
import json
import logging
import logging.handlers as log_handlers
import os

from logstash_async.constants import constants

# This is intentional
constants.ERROR_LOG_RATE_LIMIT = "0 per minute"

from logstash_async.handler import AsynchronousLogstashHandler
from xpresso.ai.core.commons.exceptions.xpr_exceptions import \
    InvalidConfigException
from xpresso.ai.core.commons.utils.constants import HUNDRED_MB_IN_BYTES
from xpresso.ai.core.commons.utils.singleton import Singleton
from xpresso.ai.core.commons.utils.xpr_config_parser import XprConfigParser


class XprLogger(logging.Logger, metaclass=Singleton):
    """
    Creates a logger object to put the logs into a file and index them into
    elastic search.
    """

    LOGGING_SECTION = "logging"
    LOG_HANDLER_SUBSECTION = "log_handler"
    LOGSTASH_HOST = "host"
    LOGSTASH_PORT = "port"
    LOGSTASH_CACHE_BOOL = "cache_in_file"
    LOGGING_LOGSTASH_BOOL = "log_to_elk"
    LOGGING_FILE_BOOL = "log_to_file"
    LOGGING_CONSOLE_BOOL = "log_to_console"
    LOGS_FOLDER_PATH = "logs_folder_path"
    FORMATTER = "formatter"
    PROJECT_NAME = "project_name"
    FIND_CONFIG_RECURSIVE = "find_config_recursive"

    FILE_BACKUP_COUNT = 5
    APPEND_MODE = 'a'

    def __init__(self, name="default",
                 config_path=XprConfigParser.DEFAULT_CONFIG_PATH,
                 level=logging.ERROR):

        self.xpr_config = XprConfigParser(config_file_path=config_path)[
            self.LOGGING_SECTION]
        if self.xpr_config[self.LOG_HANDLER_SUBSECTION][
            self.FIND_CONFIG_RECURSIVE]:
            self.xpr_config = self.load_config("xpr")

        self.name = name
        super(XprLogger, self).__init__(self.name)

        self.setLevel(level)

        logger_formatter = XprCustomFormatter(
            self.xpr_config[self.LOG_HANDLER_SUBSECTION][self.FORMATTER])
        logstash_formatter = XprLogstashCustomFormatter(
            self.xpr_config[self.LOG_HANDLER_SUBSECTION][self.FORMATTER])
        log_folder = os.path.expanduser(
            self.xpr_config[self.LOG_HANDLER_SUBSECTION][self.LOGS_FOLDER_PATH])
        if not os.path.exists(log_folder):
            try:
                os.makedirs(log_folder, 0o755)
            except IOError as err:
                print(
                    "Permission Denied to create logs folder at the specified "
                    "directory. \n{}".format(str(err)))

        # Adding file handler for levels below warning
        if self.xpr_config[self.LOG_HANDLER_SUBSECTION][self.LOGGING_FILE_BOOL]:
            if not self.set_file_handler(log_folder=log_folder,
                                         logger_formatter=logstash_formatter,
                                         level=level,
                                         extension="log"):
                print("Permission denied to create log files. "
                      "Saving log files in base directory.\n")
                self.set_file_handler(log_folder=os.path.expanduser("~"),
                                      logger_formatter=logstash_formatter,
                                      level=level,
                                      extension="log")

        # Adding file handler for levels more critical than warning
        if self.xpr_config[self.LOG_HANDLER_SUBSECTION][self.LOGGING_FILE_BOOL]:
            if not self.set_file_handler(log_folder=log_folder,
                                         logger_formatter=logstash_formatter,
                                         level=logging.ERROR,
                                         extension="err"):
                print("Warning: Failed to create log files. "
                      "Saving log files in base directory.\n")
                self.set_file_handler(log_folder=os.path.expanduser("~"),
                                      logger_formatter=logstash_formatter,
                                      level=logging.ERROR,
                                      extension="err")

        # Adding logstash logging handler
        if self.xpr_config[self.LOG_HANDLER_SUBSECTION][
            self.LOGGING_LOGSTASH_BOOL]:
            cache_filename = ""
            if self.xpr_config[self.LOG_HANDLER_SUBSECTION][
                self.LOGSTASH_CACHE_BOOL]:
                cache_filename = os.path.join(
                    log_folder, "cache.persistence")

            lh = AsynchronousLogstashHandler(
                host=self.xpr_config[self.LOG_HANDLER_SUBSECTION][
                    self.LOGSTASH_HOST],
                port=self.xpr_config[self.LOG_HANDLER_SUBSECTION][
                    self.LOGSTASH_PORT],
                database_path=cache_filename,
                event_ttl=120)
            lh.setFormatter(logstash_formatter)
            self.addHandler(lh)

        if self.xpr_config[self.LOG_HANDLER_SUBSECTION][
            self.LOGGING_CONSOLE_BOOL]:
            # Adding console logging handler
            stream_lh = logging.StreamHandler()
            stream_lh.setFormatter(logger_formatter)
            self.addHandler(stream_lh)

    def set_file_handler(self, log_folder, logger_formatter, level, extension):
        """ Set file handler for the log """
        try:
            rfh = log_handlers.RotatingFileHandler(
                filename=os.path.join(
                    log_folder,
                    '.'.join((self.xpr_config[self.PROJECT_NAME], extension))),
                mode=self.APPEND_MODE,
                maxBytes=HUNDRED_MB_IN_BYTES,
                backupCount=self.FILE_BACKUP_COUNT)
            rfh.setFormatter(logger_formatter)
            rfh.setLevel(level)
            self.addHandler(rfh)
            return True
        except IOError as err:
            # failing_silently
            pass
        return False

    def filter(self, record):
        record.projectname = self.xpr_config[self.PROJECT_NAME]
        return True

    def find_config(self, config_log_filename):
        """
        Iterates over the whole directory structure to find the path of the
        config file.

        Args:
            config_log_filename(str): name of the config file to be looked for

        Returns:
            str : Absolute Filepath of the configuration file

        """
        filepath = ""
        for dirpath, subdirs, files in os.walk(os.getcwd()):
            for x in files:
                if x.endswith(config_log_filename):
                    filepath = os.path.join(dirpath, x)
                    return filepath
        if not filepath:
            raise FileNotFoundError

    def load_config(self, config_log_type) -> XprConfigParser:
        """
        Args:
            config_log_type(str): Type of the config file i.e xpr or setup

        Returns:
            str : configuration filename based on the type provided

        """

        if config_log_type == "setup":
            config_log_filename = "setup_log.json"
        elif config_log_type == "xpr":
            config_log_filename = "xpr_log.json"
        else:
            raise ValueError("Invalid parameter passed to load_config")

        config = None
        try:
            config = XprConfigParser(
                os.path.join(self.find_config(config_log_filename)))
        except FileNotFoundError as err:
            # This is intended
            pass
        except InvalidConfigException as err:
            print(
                "Invalid config Found. Loading from the config from default"
                " path. \n{}".format(str(err)))
        finally:
            if config is None:
                try:
                    config = XprConfigParser(
                        XprConfigParser.DEFAULT_CONFIG_PATH)
                except FileNotFoundError as err:
                    print(
                        "Unable to file the config file in base directory. "
                        "Loading from the config from default path. \n"
                        "{}".format(str(err)))
                    raise err
                except InvalidConfigException as err:
                    print("Invalid config Found. \n{}".format(str(err)))
                    raise err
        return config

    def __reduce__(self):
        return self.__class__, (self.name,)


class XprCustomFormatter(logging.Formatter):
    """
    Takes the record and formats the log
    in a very specific way
    """

    def __init__(self, formatter):
        super().__init__()
        self.formatter = formatter

    def format(self, record):
        log_values = [
            datetime.datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S.%fZ'),
            "%s=%s" % ("projectname", getattr(record, "projectname"))]
        for key in self.formatter:
            if self.formatter[key]:

                try:
                    if key == "exc_info":
                        log_values.append("%s=%s" % (
                            key, self.formatException(getattr(record, key))))
                        continue
                except Exception as err:
                    continue
                log_values.append("%s=%s" % (key, getattr(record, key)))
        return " ".join(log_values)


class XprLogstashCustomFormatter(logging.Formatter):
    """
    Takes the record and formats the log
    in a very specific way
    """

    def __init__(self, formatter):
        super().__init__()
        self.formatter = formatter

    def format(self, record):
        log_values = dict()
        log_values["timestamp"] = datetime.datetime.utcnow().strftime(
            '%Y-%m-%dT%H:%M:%S.%fZ')
        log_values["projectname"] = getattr(record, "projectname")
        for key in self.formatter:
            if self.formatter[key]:
                log_values[str(key)] = str(getattr(record, key))
        return json.dumps(log_values)


if __name__ == '__main__':
    main_logger = XprLogger()
    main_logger.info("Test message")
