"""Class for distributed structured dataset"""
import copy
import os

import databricks.koalas as ks
from IPython.display import display
from py4j.protocol import Py4JJavaError

from xpresso.ai.core.commons.utils.xpr_config_parser import XprConfigParser
from xpresso.ai.core.data.automl import utility
from xpresso.ai.core.data.automl.dataset import AbstractDataset
from xpresso.ai.core.data.automl.dataset_type import DatasetType
from xpresso.ai.core.data.connections.connector import Connector
from xpresso.ai.core.data.distributed.exploration \
    .distributed_structured_dataset_info \
    import StructuredDatasetInfo
from xpresso.ai.core.data.exploration.data_type import DataType
from xpresso.ai.core.logging.xpr_log import XprLogger

__all__ = ["DistributedStructuredDataset"]
__author__ = ["Sanyog Vyawahare"]

LOGGER = XprLogger()


class DistributedStructuredDataset(AbstractDataset):
    """ StructuredDataset stores the data in tabular format. It reads data
    from csv, excel or any database. It stores the automl into local storage
    in pickle format."""

    def __init__(self, dataset_name: str = "default",
                 description: str = "This is a structured automl"):
        super().__init__(dataset_name=dataset_name,
                         description=description)

        self.info = StructuredDatasetInfo()
        self.type = DatasetType.DIST_STRUCTURED

    def import_dataset(self, user_config, local_storage_required: bool = False,
                       sample_percentage: float = 100):

        """ Fetches automl from multiple data sources and loads them
        into a automl"""
        self.data = Connector.getconnector(user_config.get("type"),
                                           user_config.get("data_source")) \
            .import_dataframe(user_config)

        self.local_storage_required = local_storage_required
        self.sample_percentage = sample_percentage

    def unique(self, attr_name=None, top=None):
        """Function to get top n unique categories
        of a categorical attribute
        Args:
            attr_name('str'): Attribute name of which unique category to be
            listed
            top('int'): Number of top categories to be listed"""
        if attr_name is None:
            print("provide attribute name")
            return
        for attr in self.info.attributeInfo:
            if attr.name != attr_name:
                continue
            if attr.type not in [DataType.NOMINAL.value,
                                 DataType.ORDINAL.value]:
                print("{} is not a categorical attribute".format(attr_name))
                return
            unique = attr.metrics["freq_count"]
            unique = ks.DataFrame(ks.Series(unique, name='unique'))
            if top is not None:
                unique = unique.head(top)
            display(unique)

    def filter(self, items=None, like=None, axis=None, regex=None):
        """Filters the automl into subset
        items('list'): Keep labels from axis which are in items.
        like('string'): Keep labels from axis for which “like in label == True”.
        regex('string'): Keep labels from axis for which re.search(regex,
        label) == True.
        axis('int'): The axis to filter on. By default
        this is ‘columns’ for DataFrame.
        """
        self.data = self.data.filter(items=items, like=like, axis=axis,
                                     regex=regex)

    def change_type(self, attribute_name, new_type):
        """Change the type of an attribute
        Args:
            attribute_name('str'): attribute whose type is to be changed
            new_type('str'): New type t assigned to the attribute"""
        attr_list = list(filter(lambda attr: attr.name == attribute_name,
                                self.info.attributeInfo))
        if not attr_list:
            print("{} attribute doesn't exist".format(attribute_name))
            return
        attr = attr_list[0]
        if new_type == DataType.NUMERIC.value and attr.type == \
                DataType.NOMINAL.value and \
                attr.dtype == DataType.FLOAT.value:
            attr.type = new_type
        elif new_type == DataType.NOMINAL.value and (
                attr.type == DataType.NUMERIC.value or
                attr.type == DataType.TEXT.value):
            attr.type = new_type
        elif new_type == DataType.TEXT.value and (
                attr.type == DataType.NOMINAL.value or
                attr.dtype == DataType.OBJECT.value):
            attr.type = new_type

        if attr.type != new_type:
            print("{} to {} not possible for {} attribute".format(attr.type,
                                                                  new_type,
                                                                  attr.name))

    def show(self, k=5):
        """Displays first k rows of the dataframe
        Args:
            k (int): Number of rows to be shown
        """
        if not len(self.data):
            # print is required,  to display on the message on the user side
            print("Unable to display empty data frame")
        print("Top {} rows".format(k))
        display(self.data.head(k))

    def save(self):
        """ Save the data into the local file system in
        a serialized format

        Returns:
            str: json file path where serialized metadata, metrics has been
            stored
            str: data_file_path where csv data has been stored
        """
        temp_data = copy.copy(self.data)
        try:
            del self.data
        except AttributeError:
            LOGGER.warning("Data attribute error")
        json_file_path = self.json_serialize_structured()
        self.data = copy.copy(temp_data)
        if not os.path.exists(json_file_path):
            print("Unable to save distributed structured {} dataset".format(self.name))
        folder_path = os.path.dirname(json_file_path)
        return folder_path

    def load(self, directory_path):
        """
        Load the data set from local storage and deserialize to update
        the dataset
        Args:
            directory_path(str): path where json file (i.e. metrics,
            metadata) and csv file (data) is stored
        """
        json_data_path = utility.get_json_from_dir(directory_path)
        self.json_deserialize_structured(json_data_path)

    def save_data(self, commit_id):
        """ Helper function to save data into hdfs
        Args:
            commit_id(str): file name to store data into hdfs """
        config_parser = XprConfigParser()
        hdfs_ip = config_parser["distributed_exploration"]["hdfs_ip"]
        ipc_port = config_parser["distributed_exploration"]["ipc_port"]
        folder_path = config_parser["distributed_exploration"]["folder_path"]
        try:
            self.data.to_parquet(os.path.join(
                "hdfs://{}:{}{}".format(hdfs_ip, ipc_port, folder_path),
                commit_id), mode="overwrite")
        except Py4JJavaError:
            LOGGER.error("Unable to save data into hdfs")

    def load_data(self, commit_id):
        """ Helper function to load data from hdfs
        Args:
            commit_id(str): commit id to retrieve data"""
        config_parser = XprConfigParser()
        hdfs_ip = config_parser["distributed_exploration"]["hdfs_ip"]
        ipc_port = config_parser["distributed_exploration"]["ipc_port"]
        folder_path = config_parser["distributed_exploration"]["folder_path"]
        try:
            self.data = ks.read_parquet(os.path.join(
                "hdfs://{}:{}{}/".format(hdfs_ip, ipc_port, folder_path),
                commit_id))
        except Py4JJavaError:
            LOGGER.error("Unable to load data from hdfs")
